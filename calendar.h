#ifndef CALENDAR_H
#define CALENDAR_H

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define CALENDAR_TEXT_LENGTH 168
#define DAYS_COUNT_IN_WEEK 7
#define MAX_DAYS_IN_MONTH 31
#define MAX_WEEK_IN_MONTH 6
#define MIN_DAYS_IN_MONTH 28
#define MONTH_TEXT_LENGTH 10

void populate_calendar(char calendar[], int month, int year, int offset_day);

#endif