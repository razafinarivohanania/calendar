#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "calendar.h"

int main()
{
    char calendar[CALENDAR_TEXT_LENGTH];
    int month = 3;
    int year = 2000;
    int offset_day = 3;

    populate_calendar(calendar, month, year, offset_day);

    printf("%s\n", calendar);
    return 0;
}