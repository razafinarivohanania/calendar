#include "calendar.h"

static int get_days_number_in_month(int month, int year);
static void int_to_string(int int_variable, char string[]);
static bool is_leap_year(int year);
static bool is_valid_offset_and_number_days(int offset_day, int days_number_in_month);
static void populate_by_spaces(char string[], int spaces_count);
static void populate_calendar_by_days(char calendar[], int days_of_month[][DAYS_COUNT_IN_WEEK]);
static void populate_days_of_month(int days_of_month[][7], int offset_day, int days_number_in_month);
static void populate_days_of_month_by_default_values(int days[][DAYS_COUNT_IN_WEEK]);
static void populate_months(char months[][MONTH_TEXT_LENGTH]);
static void populate_month_and_year(int month, int year, char month_and_year[]);
static void trim_calendar(char calendar[]);

static bool is_leap_year(int year)
{
    return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

static int get_days_number_in_month(int month, int year)
{
    if (month % 2 == 0)
    {
        if (month == 2)
        {
            if (is_leap_year(year))
            {
                return 29;
            }

            return 28;
        }

        return 30;
    }

    return 31;
}

static void int_to_string(int int_variable, char string[])
{
    sprintf(string, "%d", int_variable);
}

static bool is_valid_offset_and_number_days(int offset_day, int days_number_in_month)
{
    return offset_day >= 0 && offset_day < DAYS_COUNT_IN_WEEK && days_number_in_month >= MIN_DAYS_IN_MONTH && days_number_in_month <= MAX_DAYS_IN_MONTH;
}

static void populate_by_spaces(char string[], int spaces_count)
{
    for (int i = 0; i < spaces_count; i++)
    {
        string[i] = ' ';
    }

    string[spaces_count] = '\0';
}

void populate_calendar(char calendar[], int month, int year, int offset_day)
{
    char month_and_year[15];
    populate_month_and_year(month, year, month_and_year);

    char spaces[20];
    int space_to_add = (20 - strlen(month_and_year)) / 2;
    populate_by_spaces(spaces, space_to_add);

    spaces[space_to_add] = '\0';

    strcpy(calendar, spaces);
    strcat(calendar, month_and_year);

    space_to_add = 20 - (strlen(spaces) + strlen(month_and_year));
    populate_by_spaces(spaces, space_to_add);
    strcat(calendar, spaces);
    strcat(calendar, "\nSu Mo Tu We Th Fr Sa\n");

    int days_of_month[MAX_WEEK_IN_MONTH][DAYS_COUNT_IN_WEEK];
    populate_days_of_month(days_of_month, offset_day, get_days_number_in_month(month, year));

    populate_calendar_by_days(calendar, days_of_month);
    trim_calendar(calendar);
}

static void populate_calendar_by_days(char calendar[], int days_of_month[][DAYS_COUNT_IN_WEEK])
{
    int iteration = strlen(calendar);
    for (int i = 0; i < MAX_WEEK_IN_MONTH; i++)
    {
        for (int j = 0; j < DAYS_COUNT_IN_WEEK; j++)
        {
            char day_as_string[3];
            int_to_string(days_of_month[i][j], day_as_string);
            int day_length = strlen(day_as_string);

            if (day_length == 1 && day_as_string[0] == '0')
            {
                day_as_string[0] = ' ';
            }

            for (int k = 0; k < 3; k++)
            {
                switch (k)
                {
                case 0:
                    if (day_length == 1)
                    {
                        calendar[iteration] = ' ';
                    }
                    else
                    {
                        calendar[iteration] = day_as_string[0];
                    }
                    break;
                case 1:
                    if (day_length == 1)
                    {
                        calendar[iteration] = day_as_string[0];
                    }
                    else
                    {
                        calendar[iteration] = day_as_string[1];
                    }
                    break;
                default:
                    if (j == DAYS_COUNT_IN_WEEK - 1)
                    {
                        calendar[iteration] = '\n';
                    }
                    else
                    {
                        calendar[iteration] = ' ';
                    }
                    break;
                }
                iteration++;
            }
        }
    }

    calendar[iteration - 1] = '\0';
}

static void populate_days_of_month(int days_of_month[][DAYS_COUNT_IN_WEEK], int offset_day, int days_number_in_month)
{
    populate_days_of_month_by_default_values(days_of_month);
    if (is_valid_offset_and_number_days(offset_day, days_number_in_month))
    {
        int iteration = 0;

        for (int i = 0; i < MAX_WEEK_IN_MONTH; i++)
        {
            for (int j = 0; j < DAYS_COUNT_IN_WEEK; j++)
            {
                iteration++;

                int day_of_month = iteration - offset_day;
                if (day_of_month > 0 && day_of_month <= days_number_in_month)
                {
                    days_of_month[i][j] = day_of_month;
                }
            }
        }
    }
}

static void populate_days_of_month_by_default_values(int days[][DAYS_COUNT_IN_WEEK])
{
    for (int i = 0; i < MAX_WEEK_IN_MONTH; i++)
    {
        for (int j = 0; j < DAYS_COUNT_IN_WEEK; j++)
        {
            days[i][j] = 0;
        }
    }
}

static void populate_months(char months[][MONTH_TEXT_LENGTH])
{
    strcpy(months[0], "January");
    strcpy(months[1], "February");
    strcpy(months[2], "March");
    strcpy(months[3], "April");
    strcpy(months[4], "May");
    strcpy(months[5], "June");
    strcpy(months[6], "July");
    strcpy(months[7], "August");
    strcpy(months[8], "September");
    strcpy(months[9], "October");
    strcpy(months[10], "November");
    strcpy(months[11], "December");
}

static void populate_month_and_year(int month, int year, char month_and_year[])
{
    char months[12][10];
    populate_months(months);
    strcpy(month_and_year, months[month - 1]);

    char year_as_string[5];
    int_to_string(year, year_as_string);

    strcat(month_and_year, " ");
    strcat(month_and_year, year_as_string);
}

static void trim_calendar(char calendar[])
{
    calendar[CALENDAR_TEXT_LENGTH - 1] = '\0';

    for (int i = CALENDAR_TEXT_LENGTH - 2; i >= 0; i--)
    {
        if (calendar[i] == '\n' || calendar[i] == ' ')
        {
            continue;
        }

        if (i < CALENDAR_TEXT_LENGTH - 2)
        {
            calendar[i + 1] = '\0';
        }

        return;
    }
}